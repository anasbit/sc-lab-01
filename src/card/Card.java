package card;

public class Card {
	  
		   private double balance;

		   /**
		      Constructs a bank account with a zero balance.
		   */
		   public Card()
		   {   
		      balance = 0;
		   }

		   /**
		      Constructs a bank account with a given balance.
		      @param initialBalance the initial balance
		   */
		   public Card(double abalance)
		   {   
		      balance = abalance;
		   }

		   /**
		      Deposits money into the bank account.
		      @param amount the amount to deposit
		   */
		   public void add(double amount)
		   {  
		      double bbalance = balance + amount;
		      balance = bbalance;
		   }

		   /**
		      Withdraws money from the bank account.
		      @param amount the amount to withdraw
		   */
		   public void pay(double amount)
		   {   
		      double cbalance = balance - amount;
		      balance = cbalance;
		   }

		   /**
		      Gets the current balance of the bank account.
		      @return the current balance
		   */
		   public double getBalance()
		   {   
		      return balance;
		   }
			public String toString() {
				return balance + "  " ;
			}

		}